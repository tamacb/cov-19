import 'dart:convert';
import 'package:bubble_bottom_bar/bubble_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutterappcoronaapiidn/ui/affected_countries.dart';
import 'package:flutterappcoronaapiidn/ui/world_panel.dart';
import 'package:flutterappcoronaapiidn/view/country_page.dart';
import 'package:http/http.dart' as http;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with WidgetsBindingObserver{
  int currentIndex;
  Map worldData;
  fetchWorldData() async {
    http.Response response =
        await http.get('https://disease.sh/v3/covid-19/all?yesterday=true');
    setState(() {
      worldData = json.decode(response.body);
    });
  }

  List countryData;
  fetchcountryData() async {
    http.Response response = await http
        .get('https://disease.sh/v3/covid-19/countries?yesterday=true');
    setState(() {
      countryData = json.decode(response.body);
    });
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    fetchWorldData();
    fetchcountryData();
    super.initState();
    currentIndex = 0;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.inactive){
      print("SEDANG INACTIVE");
    }
    if (state == AppLifecycleState.paused){
      print("SEDANG PAUSED");
    }
    if (state == AppLifecycleState.resumed){
      print("SEDANG RESUME");
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.search),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
        bottomNavigationBar: BubbleBottomBar(
          items: <BubbleBottomBarItem>[
            BubbleBottomBarItem(
              icon: Icon(Icons.map),
              title: Text("world", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black)),
              backgroundColor: Colors.blueAccent,
              activeIcon: Icon(Icons.map),
            ),
            BubbleBottomBarItem(
              icon: Icon(Icons.list),
              title: Text("Country", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black),),
              backgroundColor: Colors.blueAccent,
              activeIcon: Icon(Icons.list),
            ),
            BubbleBottomBarItem(
              icon: Icon(Icons.settings),
              title: Text("Settings", style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600, color: Colors.black),),
              backgroundColor: Colors.blueAccent,
              activeIcon: Icon(Icons.settings),
            ),
          ],
          opacity: 0.2,
          backgroundColor: Colors.transparent,
          borderRadius: BorderRadius.circular(8),
          currentIndex: currentIndex,
          onTap: (int index){
            setState(() {
              currentIndex = index;
            });
          },
          hasInk: true,
          hasNotch: true,
          inkColor: Colors.blueAccent,
          fabLocation: BubbleBottomBarFabLocation.end,
        ),
        body: (currentIndex == 0) ? SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(8),
                        bottomLeft: Radius.circular(8)),
                    color: Colors.lightBlue,
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://akcdn.detik.net.id/visual/2020/07/22/ilustrasi-vaksin-corona_169.jpeg?w=650'),
                        fit: BoxFit.none)),
                height: 250,
                width: double.infinity,
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Covid-19 Tracker",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 18),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "India",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 23),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "Last Update 1 hour ago",
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 16),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.fromLTRB(12, 12, 12, 0),
                            child: Text(
                              "World",
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.w600),
                            ),
                          ),
                          Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => CountryPage()));
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.white,
                                  ),
                                  height: 30,
                                  width: 100,
                                  child: Center(child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Icon(Icons.list),
                                      Text("Regional"),
                                    ],
                                  )),
                                ),
                              ))
                        ],
                      ),
                      (worldData == null)
                          ? Center(child: CircularProgressIndicator())
                          : WorldwidePanel(
                              worldData: worldData,
                            ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 5, vertical: 5),
                            child: Text(
                              "Most Affected Countries",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          (countryData == null)
                              ? Center(child: CircularProgressIndicator())
                              : AffectedCountries(
                                  contrydata: countryData,
                                )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ) : CountryPage(),
      ),
    );
  }
}
