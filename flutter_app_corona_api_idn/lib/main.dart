import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterappcoronaapiidn/view/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: IndexPage()
    );
  }
}

class IndexPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: HomePage(),
        appBar: AppBar(
          title: Text("Corona App ina"),
          actions: [
            Center(child: Text("nama")),
            IconButton(
              icon: Icon(Icons.account_circle),
              onPressed: () {},
            ),
          ],
        ),
      );
  }
}
