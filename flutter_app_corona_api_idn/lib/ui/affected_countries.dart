import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AffectedCountries extends StatelessWidget {
  final List contrydata;

  const AffectedCountries({Key key, this.contrydata}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) => Container(
          margin: EdgeInsets.symmetric(vertical: 2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.grey.withOpacity(0.2)
          ),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.network(contrydata[index]['countryInfo']['flag'], height: 40,),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(contrydata[index]['country'], style: TextStyle(color: Colors.black),),
              ),
              Text("Deaths : "+contrydata[index]['deaths'].toString(), style: TextStyle(color: Colors.red),)
            ],
          ),
        ),
        itemCount: 10,
      ),
    );
  }
}
