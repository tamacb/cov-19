import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterappcoronaapiidn/view/home_page.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CountryPage extends StatefulWidget {
  @override
  _CountryPageState createState() => _CountryPageState();
}

class _CountryPageState extends State<CountryPage> {
  List countryData;
  fetchcountryData() async {
    http.Response response = await http
        .get('https://disease.sh/v3/covid-19/countries?yesterday=true');
    setState(() {
      countryData = json.decode(response.body);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    fetchcountryData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Countries"),
      ),
      body: (countryData==null) ? Center(child: CircularProgressIndicator()) : ListView.builder(
        itemBuilder: (context, index) => Container(
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.blueAccent.withOpacity(0.1),
            ),
            margin: EdgeInsets.all(5),
            child: Row(
              children: <Widget>[
                Image.network(
                  countryData[index]['countryInfo']['flag'],
                  height: 40,
                  width: 100,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Nama : " + countryData[index]['country'],
                      style: TextStyle(color: Colors.black),
                    ),
                    Text(
                      "Total kasus :" + countryData[index]['cases'].toString(),
                      style: TextStyle(color: Colors.black),
                    ),
                    Text(
                      "Kasus hari ini :" +
                          countryData[index]['todayCases'].toString(),
                      style: TextStyle(color: Colors.black),
                    ),
                    Text(
                      "Kasus meninggal : " +
                          countryData[index]['deaths'].toString(),
                      style: TextStyle(color: Colors.black),
                    ),
                    Text(
                      "Sembuh : " +
                          countryData[index]['recoveredPerOneMillion']
                              .toString(),
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
        itemCount: countryData == null ? 0 : countryData.length,
      ),
    );
  }
}
