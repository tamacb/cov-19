import 'dart:convert';

import 'package:http/http.dart' as http;

class ModelCorona {
  String key;
  String jumlah_kasus;
  String jumlah_sembuh;
  String jumlah_meninggal;
  String jumlah_dirawat;

  ModelCorona({this.key, this.jumlah_kasus, this.jumlah_sembuh,
      this.jumlah_meninggal, this.jumlah_dirawat});

  factory ModelCorona.createModel(Map<String, dynamic> obj ){
    return ModelCorona(
      key: obj['key'],
      jumlah_kasus: obj['jumlah_kasus'],
      jumlah_sembuh: obj['jumlah_sembuh'],
      jumlah_meninggal: obj['jumlah_meninggal'],
      jumlah_dirawat: obj['jumlah_dirawat']
    );
  }

  static Future<List<ModelCorona>> getCoronas() async {
    String apiUrl = "https://data.covid19.go.id/public/api/prov.json";

    var apiResult = await http.get(apiUrl);
    var jsonObj = json.decode(apiResult.body);
    List<dynamic> listdata = (jsonObj as Map<String, dynamic>)['list_data'];

    List<ModelCorona> coronas = [];
    for(int i = 0; i < listdata.length; i++)
      coronas.add(ModelCorona.createModel(listdata[i]));
    return coronas;
  }
}